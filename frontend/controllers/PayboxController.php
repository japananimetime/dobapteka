<?php

namespace frontend\controllers;


use common\models\Orders;
use common\models\PaymentToken;
use Yii;
use Paybox\Pay\Facade as PayboxApi;
use yii\web\NotFoundHttpException;

class PayboxController extends FrontendController
{
    public function actionIndex()
    {
        if(isset($_SESSION['payment_token'])){

            $order = Orders::findByToken();

            if($order){

                $order->deleteAccessToken();

                $telephone =  $order->user_id == null ? $order->telephone : $order->user->username;
                $email =  $order->user_id == null ? $order->email : $order->user->email;

                $component = Yii::$app->paybox;
                $component->Index($order->id, 'Оплата заказа №'.$order->id, $order->sum, preg_replace('/[^a-zA-Z0-9]/','', $telephone), $email);

                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        throw new NotFoundHttpException();
    }


    public function actionResult()
    {

        if (isset($_GET['pg_order_id']) && isset($_GET['pg_payment_id']) &&
            isset($_GET['pg_result']) && $_GET['pg_result'] == 1) {

            $model = Orders::findOne($_GET['pg_order_id']);
            if($model && $model->is_active == 0){

                date_default_timezone_set('Asia/Almaty');
                $model->is_active = 1;
                $model->payment_id = $_GET['pg_payment_id'];
                $model->statusPay = Orders::payTrue;
                $model->created = date('Y-m-d H:i:s');
                $model->save(false);

                $model->sendMessageToOurPhoneNumbers();
                $model->sendMessageToPhoneNumber();
                $model->sendMessageToCallCentre();
                $model->sendMessageToEmail();

                unset($_SESSION['basket'], $_SESSION['gift'], $_SESSION['deliveryTime'], $_SESSION['deliveryPrice'], $_SESSION['productPrice'],
                    $_SESSION['promo'], $_SESSION['promo_id'], $_SESSION['bonus'], $_SESSION['earliest'], $_SESSION['guest_data'], $_SESSION['latitude'], $_SESSION['longitude']);

            }else{
                $paybox = new PayboxApi();
                return $paybox->cancel('Ошибка');
            }

        }elseif(isset($_GET['pg_order_id']) && isset($_GET['pg_result']) && $_GET['pg_result'] == 0){
            $paybox = new PayboxApi();
        }else{
            throw new NotFoundHttpException();
        }

    }



    public function actionSuccess()
    {

        if (isset($_GET['pg_payment_id']) && isset($_GET['pg_order_id'])) {

            $model = Orders::findOne($_GET['pg_order_id']);

            if($model){

                if($model->response == 2){
                    return $this->redirect('card/gift');
                }else{
                    return $this->redirect("/card/result?token=".$model->result_token);
                }
            }
        }

        throw new NotFoundHttpException();

    }




    public function actionFailure()
    {
        return $this->redirect(Yii::$app->homeUrl);
    }



}
