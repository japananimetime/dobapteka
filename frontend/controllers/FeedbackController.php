<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.12.2019
 * Time: 16:33
 */

namespace frontend\controllers;


use common\models\City;
use common\models\Emailforrequest;
use common\models\Menu;
use common\models\Message;
use common\models\Review;
use Yii;
use yii\web\NotFoundHttpException;

class FeedbackController extends FrontendController
{

    public function actionIndex(){

        return $this->render('index');
    }



    public function actionAddReview()
    {
        if (Yii::$app->request->isAjax) {
            $model = new Review();
            if ($model->load(Yii::$app->request->post())) {
                $phoneStatus = true;
                for ($i = 0; $i < strlen($model->telephone); $i++) {
                    if ($model->telephone[$i] == '_') {
                        $phoneStatus = false;
                    }
                }
                if ($phoneStatus) {
                    if ($model->save()) {
                        return $this->sendMessage($model);
                    } else {
                        $review_error = "";
                        $review_errors = $model->getErrors();
                        foreach ($review_errors as $v) {
                            $review_error .= $v[0];
                            break;
                        }
                        return $review_error;
                    }
                } else {
                    return "Необходимо заполнить «Телефон».";
                }

            }
        }else{
            throw new NotFoundHttpException();
        }
    }



    private function sendMessage(Review $review){

        $data = Message::getFeedbackMessage();
        $email = Emailforrequest::getContent()->email;

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['name' =>  $review->name, 'surname' => $review->surname, 'telephone' => $review->telephone,
                   'email' => $review->email, 'city' => $review->city, 'comment' => $review->comment]));

        return $emailSend->send();
    }

}
