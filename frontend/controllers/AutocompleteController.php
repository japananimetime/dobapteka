<?php


namespace frontend\controllers;

use common\models\Products;
use yii\web\Controller;
use frontend\models\ProductSearch;

class AutocompleteController extends Controller
{
    function strpos_arr($haystack, $needle)
    {
        if (!is_array($needle)) $needle = array($needle);
        foreach ($needle as $what) {
            if (($pos = strpos($haystack, $what)) !== false) return $pos;
        }
        return false;
    }

    public function actionIndex()
    {
        // if (apc_exists($_GET['name_startsWith'] . 'newSearchAuto')) {
        //     return apc_fetch($_GET['name_startsWith'] . 'newSearchAuto');
        // }

        $keyword = $_GET['keyword'];


        //Чтобы вернуться к поиску без учёта ошибок, раскомментировать тут две строки

        $temp = array_column(ProductSearch::find()
            // ->where('name like \'%' . $_GET['keyword'] . '%\'')
            ->select('name')
            ->asArray()
            // ->limit(5)
            ->all(), 'name');

        // $products = [];

        // foreach ($temp as $t) {
        //     $products['users'][] = [
        //         'value' => $t,
        //         'label' => $t
        //     ];
        // }

        // print_r('<pre>');
        // print_r($temp);

        // apc_store($_GET['name_startsWith'] . 'newSearchAuto', json_encode($products), 1800);

        // return json_encode($products);

        // кратчайшее расстояние пока еще не найдено



        $shortest = -1;

        $closestArr = [];
        // проходим по словам для нахождения самого близкого варианта
        // $m_array = array_filter($temp, function ($value) use ($keywords) {
        //     return $this->strpos_arr($value, $keywords) !== false;
        // });

        foreach ($temp  as $name) {

            // вычисляем расстояние между входным словом и текущим
            $lev = levenshtein($keyword, substr($name, 0, strlen($keyword)));
            $closestArr[] = [
                'lev' => $lev,
                'name' => $name
            ];
            // проверяем полное совпадение
            if ($lev == 0) {

                // это ближайшее слово (точное совпадение)
                $closest = $name;
                $shortest = 0;

                // выходим из цикла - мы нашли точное совпадение
                // break;
            }

            // если это расстояние меньше следующего наименьшего расстояния
            // ИЛИ если следующее самое короткое слово еще не было найдено
            if ($lev <= $shortest || $shortest < 0) {
                // устанивливаем ближайшее совпадение и кратчайшее расстояние
                $closest  = $name;
                $shortest = $lev;
            }
        }

        // print_r('<pre>');

        // print_r("Вы ввели: $keyword\n");
        // if ($shortest == 0) {
        //     print_r("Найдено точное совпадение: $closest\n");
        // } else {
        //     print_r("Вы не имели в виду: $closest?\n");
        // }


        usort($closestArr, function ($a, $b) {
            return $a['lev'] - $b['lev'];
        });

        foreach (array_slice($closestArr, 0, 10) as $t) {
            $products['users'][] = [
                'value' => $t['name'],
                'label' => $t['name']
            ];
        }

        return json_encode($products);

        // print_r($t);

        // $array_isset = [];
        // $array = [];

        // $query = "name LIKE '%$keyword%'";

        // for ($i = 2; $i < strlen($keyword); $i += 2) {
        //     $val = substr($keyword, 0, $i) . '-' . substr($keyword, $i, strlen($keyword));
        //     $query .= " OR name LIKE '%$val%'";
        // }

        // $ru = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э', 'ю', 'я'];
        // for ($i = 0; $i < strlen($keyword); $i += 2) {
        //     foreach ($ru as $v) {
        //         $val = substr($keyword, 0, $i) . '' . $v . '' . substr($keyword, $i + 2, strlen($keyword));
        //         $query .= "OR name LIKE '%$val%'";
        //     }
        // }



        // $products = Products::searchByName($keyword);

        // foreach ($products as $v) {

        //     if (!in_array($v->id, $array_isset)) {
        //         $array_isset[] = $v->id;
        //         $array[] = ['value' => $v->name, 'title' => $v->name];

        //         if (count($array) >= 5) {
        //             break;
        //         }
        //     }
        // }



        // if (count($array) < 5) {

        //     $products = Products::getByQuery($query);

        //     foreach ($products as $v) {
        //         if (!in_array($v->id, $array_isset)) {
        //             $array_isset[] = $v->id;
        //             $array[] = ['value' => $v->name, 'title' => $v->name];
        //             if (count($array) >= 5) {
        //                 break;
        //             }
        //         }
        //     }
        // }



        // $json = json_encode(array('users' => $array));

        // if ($_GET['callback']) {
        //     $callback = $_GET['callback'];
        //     $json = $callback . '(' . $json . ')';
        // }

        // // apc_store($_GET['name_startsWith'] . 'actionIndex', $json, 1800);

        // return $json;
    }

    public function correctTypo($text)
    {
        $lat = [
            'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}',
            'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"',
            'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>',
        ];
        $cyr = [
            'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ',
            'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э',  'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э',
            'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю',  'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю',
        ];
        return str_replace($lat, $cyr, $text);
    }

    public function actionTemp()
    {
        \Yii::$app->redis->set('mykey', 'some value');
        echo \Yii::$app->redis->get('mykey');
    }
}