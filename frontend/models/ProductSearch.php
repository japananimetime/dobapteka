<?php

namespace frontend\models;

use common\models\Products;


class ProductSearch extends Products
{
    public $path = 'images/products/';

    public $lev;

    public static function tableName()
    {
        return 'products_search_view';
    }


    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'id']);
    }

    public function getIsHit(){
        return $this->product ? $this->product->isHit : 0;
    }


    public function getIsNew(){
        return $this->product ? $this->product->isNew : 0;
    }


    public function getTop_month(){
        return $this->product ? $this->product->top_month : 0;
    }


}
