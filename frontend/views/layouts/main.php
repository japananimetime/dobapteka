<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="yandex-verification" content="76ead80fd5d26505" />
    <meta name="yandex" content="noindex, follow"/>
    <meta name="robots" content="index, follow"/>
    <?php $this->registerCsrfMetaTags() ?>
    <link rel="shortcut icon" href="/images/logo_n.png" type="image/png">
    <link rel="canonical" href="<?=Yii::$app->request->hostInfo .'/'. Yii::$app->request->pathInfo;?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166804029-1"></script>
    <script src="//code-ya.jivosite.com/widget/NEaDajk3Ln" async></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-166804029-1');
    </script>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5H8JJ7V');</script>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5H8JJ7V"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- HEADER -->
    <?= $this->render('_modal') ?>

    <?= $this->render('_header') ?>


    <?= $content ?>


    <?= $this->render('_footer') ?>


    <?php $this->endBody() ?>

    </body>
</html>

<?php $this->endPage() ?>