<?
$this->title = 'Вы получили подарок!';
?>
<div class="main">
    <div class="main-content">
        <div class="container">
            <div class="bg-white">
                <div class="row" id = "checkResult">
                    <div class="col-sm-12">
                        <div class="popup-title">
                            <h6> Вы получили подарок! Выберите любой <br> доступный</h6>
                        </div>
                    </div>
                    <? foreach ($_SESSION['gift_for_price'] as $v):?>
                        <div class="col-sm-3 gift_for_price" data-id="<?=$v->id;?>" data-name="<?=$v->name;?>" >
                            <div class="gift-item">
                                <img src="<?=$v->getImage();?>" >
                                <p><?=$v->name;?></p>
                            </div>
                        </div>
                    <? endforeach;?>
                    <div class="col-sm-12">
                        <div class="bonus-btn">
                            <button style="display:block;margin: 40px auto;width: 200px; height: 50px;" id="save_gift">Выбрать</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

