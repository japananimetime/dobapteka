<div class="row" id="loadMoreResults">
    <input type="hidden" value="<?= $id ?>" class="category_id">
    <? $m = 0; ?>
    <? foreach ($products as $v) : ?>
        <? $m++; ?>
        <? if ($m > $per_page) break; ?>
        <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-4 wow fadeInUp']);?>
    <? endforeach; ?>
</div>

<div class="row" id="loadMoreButton">
    <? if (count($products) > $per_page) : ?>
        <div class="col-sm-12">
            <div class="collapse-wrap">
                <div class="load-more" id="load-more">
                    <button>Загрузить еще</button>
                </div>
            </div>
        </div>
    <? endif; ?>
</div>

<div class="row">
    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalog"></div>
<!--    --><?//= $this->render("_bonus_web"); ?>
</div>
