<? $m = 0; ?>
<? foreach ($products as $v) : ?>
    <? $m++; ?>
    <? if ($m < 13) : ?>
        <? if (!isMobile()) : ?>
            <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-4']);?>
        <? else : ?>
            <?=$this->render('/partials/mobile_product', ['product' => $v, 'class' => 'col-lg-3 col-md-6 col-sm-12 col-6']);?>
        <? endif;?>

    <? endif;?>
<? endforeach; ?>
