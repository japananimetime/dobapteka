<? if ($sop_tovary) : ?>
    <div class="product-title gradient-text border-top">
        <h5>Сопутствующие товары</h5>
    </div>
    <div class="owl2 owl-carousel owl-theme">
        <? foreach ($sop_tovary as $v) : ?>
            <? $product = $v->product; ?>
            <? if($product->status == 1):?>
            <div class="item">
                <? if ($v->discount) : ?>
                    <div class="sale">
                        <img src="/images/sale.svg" />
                        <div class="sale">
                            <p>-<?= $v->discount ?>%</p>
                        </div>
                    </div>
                <? endif; ?>
                <div class="top-sale-item">
                    <a href="<?=$v->getUrl();?>">
                        <img src="<?=$v->getImage();?>" >
                    </a>
                    <div class="name">
                        <a href="<?=$v->getUrl();?>">
                            <p><?= $product->name; ?></p>
                        </a>
                    </div>
                    <div class="price">
                        <? if ($v->discount):?>
                            <p><?= number_format($v->oldPrice, 0, '', ' '); ?></p>
                        <? endif;?>
                        <span class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?> </span> тг
                    </div>
                    <div class="basket-button">
                        <button class="btn-in-basket" data-id="<?= $product->id; ?>">
                            <img src="images/light-basket.png" alt="">В корзину</button>
                    </div>
                </div>
            </div>
            <? endif;?>
        <? endforeach; ?>
    </div>
<? endif; ?>
