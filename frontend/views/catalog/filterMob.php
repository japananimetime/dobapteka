<div class="row" id="loadMoreResultsMob">
    <input type="hidden" value="<?= $id ?>" class="category_id">
    <? $m = 0; ?>
    <? foreach ($products as $v) : ?>
        <? $m++; ?>
        <? if ($m > $per_page) break; ?>
        <?=$this->render('/partials/mobile_product', ['product' => $v, 'class' => 'col-lg-3 col-md-6 col-sm-12 col-6']);?>
    <? endforeach; ?>
</div>

<div class="row" id="loadMoreButtonMob">
    <? if (count($products) > $per_page) : ?>
        <div class="col-sm-12">
            <div class="collapse-wrap">
                <div class="load-more" id="load-more-mob">
                    <button data-quantity-id="<?= $m - 1; ?>">Загрузить еще</button>
                </div>
            </div>
        </div>
    <? endif; ?>
</div>
<div class="row">
    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalogMob"></div>
<!--    --><?//= $this->render("_bonus_mobile"); ?>
</div>
