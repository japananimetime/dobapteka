<div class="articles">
    <div class="container">
        <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="col-sm-12 wow slideInLeft">
                <div class="product-title gradient-text">
                    <h5><?=$model->name;?></h5>
                </div>
            </div>
        </div>
        <div class="row" >
            <?= Yii::t('app', $model->full_description, $data);?>

            <div class="col-sm-12 wow fadeInUp" id="relatedProducts"></div>

            <div class="container">
                <div class="row">

                </div>
            </div>

        </div>
    </div>
</div>
