<div class="inner-page">
    <div class="container py-5 px-5 px-sm-0">
        <?= $page->content; ?>
    </div>
</div>