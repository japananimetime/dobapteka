<div class="row">
    <? if ($fav == null) : ?>
        <div>
            <p>В избранном отсутствуют товары.</p>
        </div>
    <? endif; ?>
    <? if ($fav != null) : ?>
        <? foreach ($fav as $v) : ?>
            <div class="col-sm-4">
                <div class="catalog-item">
                    <div class="image">
                        <a href="<?=$v->getUrl();?>">
                            <img src="<?=$v->getImage();?>" alt="">
                        </a>
                    </div>
                    <div class="text-block">
                        <div class="delete-icon btn-delete-product-from-favorite" data-id="<?= $v->id; ?>">
                            &#10005
                        </div>
                        <div class="name">
                            <p><?= $v->name ?></p>
                        </div>
                        <div class="status">
                            <p><img src="/images/ok.png" alt="">
                                <? if ($v->checkRemainder()) { ?>Есть<? } else { ?>Нет<? } ?>
                                в наличии</p>
                        </div>
                        <div class="flex-one">
                            <div class="price">
                                <? if ($v->discount):?>
                                    <p>
                                        <?= number_format($v->oldPrice, 0, '', ' '); ?> тг
                                    </p>
                                <? endif;?>
                                <span class="gradient-text" style="<?= $v->discount ? 'white-space: normal !important' : '';?>">
                                    <?= number_format($v->calculatePrice, 0, '', ' '); ?> тг
                                </span>
                            </div>
                            <? if($v->checkRemainder()):?>
                                <div class="sale-basket-button">
                                    <button class="btn-in-basket" data-id="<?= $v->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                </div>
                            <? endif?>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    <? endif; ?>
</div>
