<?php require_once 'header.php'?>
<div class="main">
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow slideInLeft">
                    <div class="product-title gradient-text">
                        <h5>Связаться с нами</h5>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp">
                    <div class="review-item">
                        <div class="info">
                            <p>Анастасия</p>
                            <b>Злотина</b>
                            <span>Астана</span>
                        </div>
                        <div class="text">
                            <p>Фирма «Добрая аптека» выполняла заказ и установила в срок. Недорого. Кухонная стенка — моей мечты, из темно — коричного ольхового дерева с металлическими вставками и ручками золотистого цвета, с затемненным стеклом. Обязательно куплю такую же дочери на свадьбу!</p>
                           <div id="demo" class="collapse">
                               <p>Фирма «Добрая аптека» выполняла заказ и установила в срок. Недорого. Кухонная стенка — моей мечты, из темно — коричного ольхового дерева с металлическими вставками и ручками золотистого цвета, с затемненным стеклом. Обязательно куплю такую же дочери на свадьбу!</p>
                           </div>
                            <span  data-toggle="collapse" data-target="#demo">Показать еще</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp">
                    <div class="review-item">
                        <div class="info">
                            <p>Анастасия</p>
                            <b>Злотина</b>
                            <span>Астана</span>
                        </div>
                        <div class="text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, architecto aspernatur atque consectetur deserunt dicta dolorem, earum fuga inventore iste iure libero, mollitia necessitatibus omnis ratione sapiente sunt! Aperiam, voluptatum!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp">
                    <div class="review-item">
                        <div class="info">
                            <p>Анастасия</p>
                            <b>Злотина</b>
                            <span>Астана</span>
                        </div>
                        <div class="text">
                           <p>Фирма «Добрая аптека» выполняла заказ и установила в срок. Недорого. Кухонная стенка — моей мечты,</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp">
                    <div class="review-item">
                        <div class="info">
                            <p>Анастасия</p>
                            <b>Злотина</b>
                            <span>Астана</span>
                        </div>
                        <div class="text">
                            <p>Фирма «Добрая аптека» выполняла заказ и установила в срок. Недорого. Кухонная стенка — моей мечты, из темно — коричного ольхового дерева с металлическими вставками и ручками золотистого цвета, с затемненным стеклом. Обязательно куплю такую же дочери на свадьбу!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp">
                    <div class="review-item">
                        <div class="info">
                            <p>Анастасия</p>
                            <b>Злотина</b>
                            <span>Астана</span>
                        </div>
                        <div class="text">
                            <p>Фирма «Добрая аптека» выполняла заказ и установила в срок. Недорого. Кухонная стенка — моей мечты, из темно — коричного ольхового дерева с металлическими вставками и ручками золотистого цвета, с затемненным стеклом. Обязательно куплю такую же дочери на свадьбу!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp">
                    <div class="review-item">
                        <div class="info">
                            <p>Анастасия</p>
                            <b>Злотина</b>
                            <span>Астана</span>
                        </div>
                        <div class="text">
                            <p>Фирма «Добрая аптека» выполняла заказ и установила в срок. Недорого. Кухонная стенка — моей мечты, из темно — коричного ольхового дерева с металлическими вставками и ручками золотистого цвета, с затемненным стеклом. Обязательно куплю такую же дочери на свадьбу!</p>
                        </div>
                    </div>
                </div>
                <div class="offset-lg-2 col-lg-8 col-sm-12 space-top-sm wow fadeInUp">
                    <form action="">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="images/user-icon-light.png" alt="">
                                    </div>
                                    <input type="text" placeholder="ФИО">
                                </div>
                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="images/phone-icon.png" alt="">
                                    </div>
                                    <input type="text" placeholder="Номер телефона">
                                </div>
                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="images/msg-icon.png" alt="">
                                    </div>
                                    <input type="text" placeholder="Ваш e-mail">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="review">
                                    <label for="textarea"><img src="images/review.png" alt="">Отзыв</label>
                                    <textarea id="textarea" name=""></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="review-btn">
                                    <button>Оставить отзыв</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once 'footer.php'?>
