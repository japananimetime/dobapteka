<?php require_once 'header.php' ?>
<div class="main">

    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow slideInLeft">
                        <div class="main-title gradient-text">
                            <h5>Йодомарин 100 мкг, №100, табл.</h5>
                        </div>
                    </div>
                    <div class="col-sm-5 wow fadeInLeft">
                        <div class="product-img">
                            <img src="images/product-img.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-sm-7 wow fadeInRight">
                        <div class="product-description">
                            <ul>
                                <li>
                                    <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                </li>
                                <li>
                                    <p>Страна</p><span>Германия</span>
                                </li>
                                <li>
                                    <p>Модель</p><span>4013054005309</span>
                                </li>
                            </ul>
                        </div>
                        <div class="button-wrap">
                            <div class="save-btn">
                                <button>В избранное</button>
                            </div>
                            <div class="basket-button">
                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-sm-12 wow fadeInUp">
                        <div class="instruction">
                            <p data-toggle="collapse" data-target="#demo"><img src="images/collapse-arrow.png" alt="">Инструкция по применению</p>
                        </div>
                        <div id="demo" class="collapse">
                            <div class="collapse-text">
                                <h6>Форма выпуска, состав и упаковка</h6>
                                <p>Таблетки белого или почти белого цвета, круглые, плоскоцилиндрические, с фаской и риской с одной стороны. 1 таб. калия йодид 131 мкг, что соответствует содержанию йода 100 мкг Вспомогательные вещества: лактозы моногидрат - 75.119 мг, магния карбонат основной - 28.25 мг, желатин - 4 мг, карбоксиметилкрахмал натрия (тип А) - 4.75 мг, кремния диоксид коллоидный - 1.75 мг, магния стеарат - 1 мг. 50 шт. - флаконы (1) - пачки картонные. 100 шт. - флаконы (1) - пачки картонные.</p>
                                <h6>Фармакологическое действие</h6>
                                <p>Препарат йода для лечения и профилактики заболеваний щитовидной железы. Йод является жизненно важным микроэлементом, необходимым для нормальной работы щитовидной железы. Тиреоидные гормоны выполняют множество жизненно важных функций, в т.ч. регулируют обмен белков, жиров, углеводов и энергии в организме, деятельность головного мозга, нервной и сердечно-сосудистой систем, половых и молочных желез, а также рост и развитие ребенка. </p>
                                <p>Применение препарата Йодомарин® восполняет дефицит йода в организме, препятствуя развитию йододефицитных заболеваний, способствует нормализации функции щитовидной железы, что особенно важно для детей и подростков, а также при беременности и в период лактации.</p>
                                <h6>Фармакокинетика</h6>
                                <p>Данные о фармакокинетике препарата Йодомарин® не предоставлены.</p>
                                <h6>Показания</h6>
                                <ul>
                                    <li>профилактика эндемического зоба (особенно у детей, подростков, у женщин при беременности и в период грудного вскармливания);</li>
                                    <li>профилактика рецидива зоба после его хирургического удаления или после окончания медикаментозного лечения препаратами гормонов щитовидной железы;</li>
                                    <li>лечение диффузного эутиреоидного зоба, вызванного дефицитом йода у детей, подростков и у взрослых в возрасте до 40 лет. Режим дозирования</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="main-title gradient-text border-top">
                            <h5>Сопутствующие товары</h5>
                        </div>
                    </div>
                    <div class="owl1 owl-carousel owl-theme">
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img1.png" alt="">
                                <div class="name">
                                    <p>Йодомарин 10 табл.</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img3.png" alt="">
                                <div class="name">
                                    <p>Doliva маска расслабляющая 30 мл</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img1.png" alt="">
                                <div class="name">
                                    <p>Йодомарин 10 табл.</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img3.png" alt="">
                                <div class="name">
                                    <p>Doliva маска расслабляющая 30 мл</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="product-title gradient-text border-top">
                            <h5>Аналоги лекарственных средств</h5>
                        </div>
                    </div>
                    <div class="owl1 owl-carousel owl-theme">
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img1.png" alt="">
                                <div class="name">
                                    <p>Йодомарин 10 табл.</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img3.png" alt="">
                                <div class="name">
                                    <p>Doliva маска расслабляющая 30 мл</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img1.png" alt="">
                                <div class="name">
                                    <p>Йодомарин 10 табл.</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="top-sale-item">
                                <img src="images/img3.png" alt="">
                                <div class="name">
                                    <p>Doliva маска расслабляющая 30 мл</p>
                                </div>
                                <div class="price">
                                    <p class="gradient-text">369 <span> тг.</span></p>
                                </div>
                                <div class="basket-button">
                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="product-title gradient-text">
                            <h5>Йодомарин 100 мкг, №100, табл.</h5>
                        </div>
                        <div class="product-img">
                            <img src="images/product-img.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="product-description">
                            <ul>
                                <li>
                                    <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                </li>
                                <li>
                                    <p>Страна</p><span>Германия</span>
                                </li>
                                <li>
                                    <p>Модель</p><span>4013054005309</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-9 col-sm-12">
                            <div id="demo" class="collapse">
                                <div class="collapse-text">
                                    <h6>Форма выпуска, состав и упаковка</h6>
                                    <p>Таблетки белого или почти белого цвета, круглые, плоскоцилиндрические, с фаской и риской с одной стороны. 1 таб. калия йодид 131 мкг, что соответствует содержанию йода 100 мкг Вспомогательные вещества: лактозы моногидрат - 75.119 мг, магния карбонат основной - 28.25 мг, желатин - 4 мг, карбоксиметилкрахмал натрия (тип А) - 4.75 мг, кремния диоксид коллоидный - 1.75 мг, магния стеарат - 1 мг. 50 шт. - флаконы (1) - пачки картонные. 100 шт. - флаконы (1) - пачки картонные.</p>
                                    <h6>Фармакологическое действие</h6>
                                    <p>Препарат йода для лечения и профилактики заболеваний щитовидной железы. Йод является жизненно важным микроэлементом, необходимым для нормальной работы щитовидной железы. Тиреоидные гормоны выполняют множество жизненно важных функций, в т.ч. регулируют обмен белков, жиров, углеводов и энергии в организме, деятельность головного мозга, нервной и сердечно-сосудистой систем, половых и молочных желез, а также рост и развитие ребенка. </p>
                                    <p>Применение препарата Йодомарин® восполняет дефицит йода в организме, препятствуя развитию йододефицитных заболеваний, способствует нормализации функции щитовидной железы, что особенно важно для детей и подростков, а также при беременности и в период лактации.</p>
                                    <h6>Фармакокинетика</h6>
                                    <p>Данные о фармакокинетике препарата Йодомарин® не предоставлены.</p>
                                    <h6>Показания</h6>
                                    <ul>
                                        <li>профилактика эндемического зоба (особенно у детей, подростков, у женщин при беременности и в период грудного вскармливания);</li>
                                        <li>профилактика рецидива зоба после его хирургического удаления или после окончания медикаментозного лечения препаратами гормонов щитовидной железы;</li>
                                        <li>лечение диффузного эутиреоидного зоба, вызванного дефицитом йода у детей, подростков и у взрослых в возрасте до 40 лет. Режим дозирования</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="instruction">
                                <p data-toggle="collapse" data-target="#demo"><img src="images/collapse-arrow.png" alt="">Инструкция по применению</p>
                            </div>

                        </div>
                        <div class="button-wrap">
                            <div class="save-btn">
                                <button>В избранное</button>
                            </div>
                            <div class="basket-button">
                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="product-title gradient-text border-top">
                            <h5>Сопутствующие товары</h5>
                        </div>
                        <div class="owl2 owl-carousel owl-theme">
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img1.png" alt="">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img3.png" alt="">
                                    <div class="name">
                                        <p>Doliva маска расслабляющая 30 мл</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img1.png" alt="">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img3.png" alt="">
                                    <div class="name">
                                        <p>Doliva маска расслабляющая 30 мл</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="product-title gradient-text border-top">
                            <h5>Аналоги лекарственных средств</h5>
                        </div>
                        <div class="owl2 owl-carousel owl-theme">
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img1.png" alt="">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img3.png" alt="">
                                    <div class="name">
                                        <p>Doliva маска расслабляющая 30 мл</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img1.png" alt="">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="top-sale-item">
                                    <img src="images/img3.png" alt="">
                                    <div class="name">
                                        <p>Doliva маска расслабляющая 30 мл</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span> тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php require_once 'footer.php' ?>