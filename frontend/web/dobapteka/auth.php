<?php require_once 'header.php'?>
<div class="main">
    <div class="desktop-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <div class="register-title text-center gradient-text">
                            <h5>Авторизация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Войти c помощью: <a href=""><img src="images/soc3.png" alt=""></a> <a href=""><img src="images/soc4.png" alt=""></a></p>
                        </div>
                    </div>

                    <div class="offset-lg-4 col-lg-4 col-sm-12 wow fadeInUp">
                        <div class="personal-input">
                            <div class="icon">
                                <img src="images/user-icon-light.png" alt="">
                            </div>
                            <input type="text"  placeholder="+7(___)">
                        </div>
                        <div class="personal-input">
                            <div class="icon">
                                <img src="images/lock-icon.png" alt="">
                            </div>
                            <input type="password" id="myInput" placeholder="Пароль">
                            <button class="show-pass" onclick="showPass()"><img src="images/show-icon.png" alt=""></button>
                        </div>
                        <div class="button-wrap">
                            <div class="basket-button">
                                <button>Войти</button>
                            </div>
                            <div class="save-btn">
                                <button>Зарегистрироваться </button>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="register-bonus">
                            <p>Начисление бонусов при регистрации</p>
                            <p><span class="gradient-text">500</span> тг!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="register-title text-center gradient-text wow fadeInUp">
                            <h5>Авторизация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Войти c помощью:</p>
                            <div class="row">
                                <div class="col left">
                                    <a href=""><img src="images/soc-lg.png" alt=""></a>
                                </div>
                                <div class="col right">
                                    <a href=""><img src="images/soc-lg2.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="offset-lg-4 col-lg-4 col-sm-12 wow fadeInUp">
                        <div class="personal-input">
                            <div class="icon">
                                <img src="images/user-icon-light.png" alt="">
                            </div>
                            <input type="text"  placeholder="+7(___)">
                        </div>
                        <div class="personal-input">
                            <div class="icon">
                                <img src="images/lock-icon.png" alt="">
                            </div>
                            <input type="password" id="myInput2" placeholder="Пароль">
                            <button class="show-pass" onclick="showPass2()"><img src="images/show-icon.png" alt=""></button>
                        </div>
                        <div class="button-wrap2">
                            <div class="save-btn">
                                <button>Войти</button>
                            </div>
                            <div class="basket-button">
                                <button>Зарегистрироваться</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="register-bonus">
                            <p>Начисление бонусов при регистрации</p>
                            <p><span class="gradient-text">500</span> тг!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once 'footer.php'?>
