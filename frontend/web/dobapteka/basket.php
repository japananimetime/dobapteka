<?php require_once 'header.php' ?>
<div class="main">
    <div class="tab-line">
        <div class="container">
            <div class="basket-title wow slideInLeft">
                <h5 class="gradient-text">Корзина</h5>
                <div class="count">
                    2
                </div>
            </div>
        </div>
    </div>
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-8">
                    <div class="row ">
                        <div class="col-sm-6 wow fadeInUp">
                            <div class="basket-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>

                                    <div class="flex-one">
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="count-input">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                                            <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                                        </div>
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInUp">
                            <div class="basket-item">
                                <div class="image">
                                    <img src="images/img3.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Энтерожермина 5 мл</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="count-input">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                                            <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                                        </div>
                                        <div class="price">
                                            <p class="gradient-text">1200 <span> тг.</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInUp">
                            <div class="basket-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>

                                    <div class="flex-one">
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="count-input">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                                            <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                                        </div>
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInUp">
                            <div class="basket-item">
                                <div class="image">
                                    <img src="images/img3.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Энтерожермина 5 мл</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="count-input">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                                            <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                            <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                                        </div>
                                        <div class="price">
                                            <p class="gradient-text">1200 <span> тг.</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="basket-title ">
                                <h6>Расчет стоимости доставки</h6>
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInUp">
                            <div class="basket-select">
                                <select name="" id="">
                                    <option value="">Доставка по Алматы</option>

                                </select>
                            </div>
                            <div class="basket-select">
                                <select name="" id="">
                                    <option value="">Выбор оплаты</option>
                                    <option value="">Курьеру</option>
                                    <option value="">Банковская карта</option>
                                </select>
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/advantage-icon2.png" alt="">
                                </div>
                                <input type="password" placeholder="Промокод">
                                <button class="green-btn">Проверить</button>
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInUp">
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/loc-icon.png" alt="">
                                </div>
                                <input type="text" placeholder="Адрес доставки">
                            </div>
                            <div class="delivery-cost">
                                <p>Сумма за доставку:</p>
                                <div class="price">
                                    <p class="gradient-text">500 <span> тг</span></p>
                                </div>
                            </div>
                            <div class="sdacha">
                                <p>Подготовить сдачу с суммы</p>
                                <input type="text"><span class="gradient-text">тг</span>
                            </div>

                        </div>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="pay-btn">
                                        <button>Оплатить</button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="bonus-cost opacity-bonus">
                                        <p>Оплата бонусами</p>
                                        <div class="price">
                                            <p>
                                                1000
                                            </p>
                                        </div>
                                    </div>
                                    <div class="total-cost">
                                        <p>Итого:</p>
                                        <div class="price">
                                            <p class="gradient-text">2069 <span> тг</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
</div>
<?php require_once 'footer.php' ?>