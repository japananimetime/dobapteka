<?php
return [
    'PhoneMessageUsername' => "aidake",
    'PhoneMessagePassword' => "T28uECIvQ",
    'default_title' => 'Интернет аптека Добрая',
    'default_description' => '«Интернет аптека Добрая. Доставка (24/7). Поиск лекарств»',
    'default_keyword'  => 'Интернет аптека Добрая',

    'default_category_title' => '{name} - купить онлайн лекарства в Алматы и Казахстане | Добрая Аптека',
    'default_category_description' => 'Купить {name} по цене от {min_price} в ฀ Доброй аптеке ฀ ✅ Доставка по Алматы в течении 2 
    часов ✅ В наличии {amount} товаров ☎ +7 (747) 094-13-00',

    'default_product_title' => '{name} - купить онлайн в Алматы и Казахстане, инструкция, цена, состав | Добрая Аптека',
    'default_product_description' => 'Купить {name} по цене {price} в ฀ Доброй аптеке ฀ ✅ Доставка по Алматы в течении 2 
    часов ✅ В наличии ☎ +7 (747) 094-13-00',
];
