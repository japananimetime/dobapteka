<?php

namespace frontend\helpers;

use Yii;

class Url
{
    public static function force_lowercase_urls() {

        // Grab requested URL
        $url = $_SERVER['REQUEST_URI'];
        // If URL contains a period, halt (likely contains a filename and filenames are case specific)
        if ( preg_match('/[\.]/', $url) ) {
            return;
        }
        // If URL contains a question mark, halt (likely contains a query variable)
        if ( preg_match('/[\?]/', $url) ) {
            return;
        }
        if ( preg_match('/[A-Z]/', $url) ) {
            // Convert URL to lowercase
            $lc_url = strtolower($url);
            // 301 redirect to new lowercase URL
            header('Location: ' . $lc_url, TRUE, 301);
            exit();
        }
    }

    public static function delete_last_slash(){


        $url = Yii::$app->request->hostInfo .'/'. Yii::$app->request->pathInfo;
        if(substr($url, -1) == '/' && Yii::$app->request->pathInfo != null && !isset($_GET['tab'])){
            $url = substr_replace($url, "", -1);
            header('Location: ' . $url, TRUE, 301);
            exit();
        }
    }


    public static function redirect_some_urls(){

        $host = Yii::$app->request->hostInfo;
        $url = Yii::$app->request->pathInfo;

        if($url == 'catalog/podguzniki-dlya-vzrosredstva-3251' || $url == 'product/aveloks-16mg-ml-250ml-r-r-d-inf_10147' ||
            $url == 'product/arpeflyu-100mg-tab-20_10675' || $url == 'product/atsesoly-400ml-r-r-d-i-fl_10292' ||
            $url == 'product/veroshpiron-100mg-kaps-30_10511' || $url == 'catalog/lekarstvennie-sredstva'){
            header('Location: ' . $host.'/catalog/lekarstvennie-sredstva-3251', TRUE, 301);
            exit();
        }elseif(isset($_GET['name']) && $_GET['name'] == 'nuk'){
            header('Location: ' . $host.'/product/13725/Nuk-aspirator-dlya-chistki-nosa--2-nasadki-REL1_13725', TRUE, 301);
            exit();
        }elseif(isset($_GET['name']) && $_GET['name'] == 'аквадетрим'){
            header('Location: ' . $host.'/product/12994/akvadetrim--vitamin-d3--15000me-ml-10ml-fl-kap_12994', TRUE, 301);
            exit();
        }elseif(isset($_GET['name']) && $_GET['name'] == 'Мелаксен'){
            header('Location: ' . $host.'/product/12352/melaksen-3mg-tab-24_12352', TRUE, 301);
            exit();
        }elseif(isset($_GET['name']) && $_GET['name'] == 'детримакс'){
            header('Location: ' . $host.'/product/12212/detrimaks-2000-me-60-tabl_12212', TRUE, 301);
            exit();
        }elseif(isset($_GET['name']) && $_GET['name'] == 'эссенциале'){
            header('Location: ' . $host.'/product/19146/essentsiale-250mg-5ml-amp-5_19146', TRUE, 301);
            exit();
        }elseif($url == 'product/14620/tsef-iii-v-v-i-v-m-por-1g-d-i-fl_14620'){
            header('Location: ' . $host.'/product/4184224/tsef-iiir-250mg-por-d-i', TRUE, 301);
            exit();
        }elseif($host == 'https://www.dobraya-apteka.kz'){
            header('Location: ' . 'https://dobraya-apteka.kz/', TRUE, 301);
            exit();
        }

    }
}