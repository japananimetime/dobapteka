<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Geocoords".
 *
 * @property int $id
 * @property string $coords
 * @property string $name
 * @property int $summ
 */


class Geocoords extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'geocoords';
    }

    public function rules()
    {
        return [
            [['coords', 'summ', 'name'], 'required'],
            [['coords', 'name'], 'string'],
            [['summ'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coords' => 'Координаты',
            'summ' => 'Цена доставки (тг)',
            'name' => 'Время доставки (м)'
        ];
    }

    public static function getAll(){
        return Geocoords::find()->all();
    }



}
