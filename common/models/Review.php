<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Review".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $city
 * @property string $telephone
 * @property string $email
 * @property string $comment
 * @property string $created_at
 * @property int $isRead
*/


class Review extends \yii\db\ActiveRecord
{
    const isRead = 1;
    const isNotRead  = 0;

    public static function tableName()
    {
        return 'review';
    }

    public function rules()
    {
        return [
            [['name', 'surname', 'city', 'telephone', 'email'], 'required'],
            [['comment'], 'string'],
            [['isRead'], 'integer'],
            [['name', 'surname', 'email', 'telephone', 'city'], 'string', 'max' => 255],
            [['email'],'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'E-mail',
            'telephone' => 'Телефон',
            'city' => 'Город',
            'comment' => 'Сообщение',
            'isRead' => 'Статус',
            'created_at' => 'Дата создание'
        ];
    }

    public function isReaded(){
        $this->isRead = self::isRead;
        return $this->save(false);
    }

    public static function getAll(){
        return self::find()->all();
    }
}
