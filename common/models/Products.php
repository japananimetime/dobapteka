<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "Products".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $images
 * @property int $price
 * @property int $bonus
 * @property int $top_month
 * @property int $status
 * @property int $isHit
 * @property int $isNew
 * @property string $name
 * @property int $sort
 * @property int $sort_top_month
 * @property int $sort_hit
 * @property int $discount
 * @property int $level
 * @property int $category_id
 * @property string $created_at
 * @property string $url
 * @property string $text
 *
 *
 */

class Products extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/products/';
    public $count;

    const HIT_PRODUCT_LOAD_QUANTITY = 8;
    const TOP_MONTH_PRODUCT_LOAD_QUANTITY = 8;

    public static function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return [
            [['title', 'name', 'sort', 'status', 'price', 'created_at'], 'required'],
            [['sort', 'status', 'price', 'top_month', 'bonus', 'isHit', 'isNew', 'category_id'], 'integer'],
            [['category_id', 'description', 'images', 'text', 'ware_id'], 'string'],
            [['created_at'], 'safe'],
            ['discount', 'integer', 'integerOnly'=>true, 'min'=>0],
            [['title', 'name', 'url', 'status_products', 'model', 'country'], 'string', 'max' => 255],
            ['files', 'each', 'rule' => ['image']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'name' => 'Название',
            'url' => 'Ссылка',
            'images' => 'Фото',
            'top_month' => 'Товар месяца',
            'isHit' => 'Топ продаж',
            'isNew' => 'Новинка',
            'discount' => 'Скидка',
            'sort' => 'Сортировка',
            'status' => 'В наличие',
            'status_products' => 'Статус',
            'created_at' => 'Дата создание',
            'price' => 'Цена',
            'bonus' => 'Бонус',
            'text' => 'Инструкция',
            'country' => 'Производитель'
        ];
    }

    public function upload()
    {
        $time = time();
        $this->images->saveAs($this->path . $time . $this->images->baseName . '.' . $this->images->extension);
        return $time . $this->images->baseName . '.' . $this->images->extension;
    }

    public static function getAll()
    {
        return Products::find()
            ->distinct()
            ->select('products.id, products.url, products.isHit, products.images, products.name, 
            products.price, products.discount, products.top_month, products.status_products, products.isNew')
            ->select('products.*')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.amount > 0')
            ->all();
    }


    public static function getSomeHitProducts($quantity)
    {
        return Products::find()
            ->distinct()
            ->select('products.id, products.url, products.isHit, products.images, products.name, 
            products.price, products.discount, products.top_month, products.status_products, products.isNew, products.sort_hit')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.amount > 0')
            ->andWhere(['products.isHit' => 1])
            ->offset($quantity)
            ->orderBy([new \yii\db\Expression('sort_hit IS NULL ASC, sort_hit ASC')])
            ->all();
    }

    public static function getSomeTopMonthProducts($quantity)
    {
        return Products::find()
            ->distinct()
            ->select('products.id, products.url, products.isHit, products.images, products.name, 
            products.price, products.discount, products.top_month, products.status_products, products.isNew, products.sort_top_month')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.amount > 0')
            ->andWhere(['products.top_month' => 1])
            ->offset($quantity)
            ->orderBy([new \yii\db\Expression('sort_top_month IS NULL ASC, sort_top_month ASC')])
            ->all();
    }


    public static function searchByName($keyword)
    {

        // if(apc_exists($keyword . 'searchByName')){
        //     return apc_fetch($keyword . 'searchByName');
        // }

        $result = Products::find()
            ->distinct()
            ->select('products.id, products.url, products.isHit, products.images, products.name, 
            products.price, products.discount, products.top_month, products.status_products, products.ware_id')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.amount > 0')
            ->andWhere("products.name LIKE '%$keyword%'")
            ->all();

        // apc_store($keyword . 'searchByName', $result, 1800);

        return $result;
    }

    public static function getByQuery($query)
    {

        // if(apc_exists($query . 'getByQuery')){
        //     return apc_fetch($query . 'getByQuery');
        // }


        $result = Products::find()
            ->distinct()
            ->select('products.id, products.url, products.isHit, products.images, products.name, 
            products.price, products.discount, products.top_month, products.status_products')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.amount > 0')
            ->andWhere($query)
            ->all();

        // apc_store($query . 'getByQuery', $result, 1800);

        return $result;
    }


    public static function getList()
    {
        return ArrayHelper::map(Products::find()->all(), 'id', 'name');
    }


    public function generateCyrillicToLatin()
    {
        $cyr = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',
            'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
            ',', '.', ' ', '/', '(', ')', '|', '_'
        ];
        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
            '-', '-', '-', '-', '-', '-', '-', '-'
        ];
        $text = str_replace($cyr, $lat, $this->name);
        if (substr($text, -1) == '-') $text = substr($text, 0, -1);
        return preg_replace('/[^a-zA-Z0-9-]/', '', $text);
    }





    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $model = Products::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getCategory()
    {
        return $this->hasOne(CatalogProducts::className(), ['id' => 'category_id']);
    }

    public function getThis_country()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    public function getSop_tovary()
    {
        return $this->hasOne(SopTovary::className(), ['product1' => 'id']);
    }


    public static function getSum()
    {
        $sum = 0;
        if ($_SESSION['basket'] != null) {
            foreach ($_SESSION['basket'] as $v) {
                $price = (int) $v->calculatePrice;
                $sum += (int) $v->count * $price;
            }
        }

        $sum = $sum * ((100 - $_SESSION['earliest']) / 100);
        if (isset($_SESSION['promo_id'])) {
            $sum = $sum * ((100 - $_SESSION['promo']) / 100);
        }

        return intval($sum);
    }



    public static function getSumProduct($id)
    {
        $sum = 0;
        if ($_SESSION['basket'] != null) {
            foreach ($_SESSION['basket'] as $v) {
                if ($v->id == $id) {
                    $price = (int) $v->calculatePrice;
                    $sum += (int) $v->count * $price;
                }
            }
        }
        return intval($sum);
    }

    public static function getOrderProductsAsArray()
    {
        $response = array();
        foreach ($_SESSION['basket'] as $v) {
            $response[$v->id] = $v->id;
        }
        return $response;
    }


    public static function getSearchProducts()
    {
        $products = Products::find()->all();
        $result = "";
        foreach ($products as $v) {
            $result .= $v->name . ',';
        }
        $result = substr($result, 0, -1);
        return $result;
    }

    public function getCalculatePrice()
    {
        return intval($this->price);
    }


    public function getOldPrice()
    {
        if ($this->discount) {
            $price = $this->price / ((100 - $this->discount) / 100);
        } else {
            $price =  $this->price;
        }

        return intval($price);
    }

    public function getImage(){

        $img = preg_replace_callback ( '!s:(\d+):"(.*?)";!', function($match) {
            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
        },$this->images );

        $img = unserialize($img);
        if ( $img === false ){
            return null;
        }else{
            return '/backend/web/'.$this->path.$img[0];
        }
    }


    public function getUrl(){
        return '/product/'.$this->id.'/'.$this->url;
    }

    public static function getSimilarProducts($text)
    {
        return $product = Products::find()->where("name LIKE '$text%'")->all();
    }


    public function getFavoriteStatus()
    {
        if (UserFavorites::findOne(['user_id' => Yii::$app->user->id, 'product_id' => $this->id])) return 1;
        else return 0;
    }


    public function getRemainder()
    {
        return $this->hasMany(FilialProduct::className(), ['product_id' => 'id']);
    }

    public function checkRemainder(){
        return FilialProduct::find()
            ->distinct()
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.product_id = '.$this->id)
            ->andWhere('filial_product.amount > 0')
            ->exists();
    }


}
