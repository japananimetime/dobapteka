<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "SearchBanner".
 *
 * @property int $id
 * @property string $image
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 */


class SearchBanner extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/search-banner/';

    public static function tableName()
    {
        return 'search_banner';
    }

    public function rules()
    {
        return [
            [['url'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'url' => 'Ссылка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }


    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public static function getContent(){
        return self::find()->select('id, image, url')->one();
    }


}
