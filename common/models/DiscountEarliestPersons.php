<?php
namespace common\models;

use DateTime;
use Yii;


/**
 * This is the model class for table "DiscountEarliestPersons".
 *
 * @property int $id
 * @property int $age
 * @property int $discount
 */


class DiscountEarliestPersons extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'discount_earliest_persons';
    }

    public function rules()
    {
        return [
            [['age', 'discount'], 'required'],
            [['age', 'discount'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'age' => 'Возраст от',
            'discount' => 'Скидка',
        ];
    }

    public static function getContent(){
        return DiscountEarliestPersons::find()->one();
    }

    public function getDiscountPercent(){

        $profile = UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
        $birthDate = $profile->date_of_birth;
        try {
            $date = new DateTime($birthDate);
        } catch (\Exception $e) {
        }
        try {
            $now = new DateTime();
        } catch (\Exception $e) {
        }
        $interval = $now->diff($date);
        if($interval->y > $this->age){
            return $this->discount;
        }else{
            return 0;
        }
    }


}
