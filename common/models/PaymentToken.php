<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "PaymentToken".
 *
 * @property int $id
 * @property int $paymentMethod
 * @property int $deliveryMethod
 * @property int $change
 * @property int $used_bonus
 * @property int $used_promo
 * @property int $deliveryPrice
 * @property int $sum
 * @property int $response
 * @property string $created_at
 * @property string $address
 * @property string $token
 * @property string $telephone
 * @property string $email
 */

class PaymentToken extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'payment_tokens';
    }

    public function rules()
    {
        return [
            [['token', 'sum', 'used_bonus', 'used_promo', 'paymentMethod', 'telephone', 'email'], 'required'],
            [['sum', 'used_bonus', 'used_promo', 'change', 'deliveryPrice', 'paymentMethod',
                'deliveryMethod', 'response'], 'integer'],
            [['created_at'], 'safe'],
            [['token', 'address', 'telephone', 'email'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'sum' => 'Sum',
            'used_bonus' => 'Used Bonus',
            'used_promo' => 'Used Promo',
            'change' => 'Change',
            'address' => 'Address',
            'deliveryPrice' => 'Delivery Price',
            'paymentMethod' => 'Payment Method',
            'deliveryMethod' => 'Delivery Method',
            'created_at' => 'Created At',
        ];
    }


    public function generateToken(){
        $this->token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function saveResponse($response){
        $this->response = $response;
        $this->save(false);
    }

    public function deleteToken(){
        $this->delete();
        unset($_SESSION['payment_token']);
    }

    public static function findByToken($token){
        return self::findOne(['token' => $token]);
    }
}
