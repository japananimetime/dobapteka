<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "UserAddress".
 *
 * @property int $id
 * @property string $address
 * @property int $user_id
 */

class UserAddress extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_address';
    }

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['user_id'], 'integer'],
            [['address'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'address' => 'Адрес',
        ];
    }

    public static function getUserAddresses(){
        return self::findAll(['user_id' => Yii::$app->user->id]);
    }
}
