<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "PromoUsers".
 *
 * @property int $id
 * @property int $user_id
 * @property int $promo_id
 */

class PromoUsers extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'promo_users';
    }

    public function rules()
    {
        return [
            [['user_id', 'promo_id'], 'required'],
            [['user_id', 'promo_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'promo_id' => 'Promo ID',
        ];
    }
}
