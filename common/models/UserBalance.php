<?php
namespace common\models;

use frontend\controllers\CardController;
use Yii;

/**
 * This is the model class for table "UserBalance".
 *
 * @property int $id
 * @property int $user_id
 * @property int $sum
 */

class UserBalance extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_balance';
    }

    public function rules()
    {
        return [
            [['user_id', 'sum'], 'required'],
            [['user_id', 'sum'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sum' => 'Sum',
        ];
    }


    public static function createUserBalanceForRegistration($user_id){
        $balance = new UserBalance();
        $balance->user_id = $user_id;
        $balance->sum = 500;
        return $balance->save();
    }


    public static function addBonus($bonus){

        $userBonus = UserBalance::findOne(['user_id'=>Yii::$app->user->id]);

        // add 3% bonus from basket
//        $sum = CardController::getSumBasketWithoutDelivery();
//        $bonus = $sum * (3/100) + $bonus;
        // end add 3% bonus from basket

        if($userBonus){
            $userBonus->sum += $bonus;
            $userBonus->save(false);
        }

    }
}
