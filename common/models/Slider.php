<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Slider".
 *
 * @property int $id
 * @property string $image
 * @property string $url
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */


class Slider extends \yii\db\ActiveRecord
{
    const is_active = 1;
    const not_active = 1;

    public $path = 'images/slider/';

    public static function tableName()
    {
        return 'sliders';
    }

    public function rules()
    {
        return [
            [['status'], 'required'],
            [['url'], 'string'],
            [['status'], 'number'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'url' => 'Ссылка',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public static function getAll(){
        return self::find()
            ->where(['status' => self::is_active])
            ->orderBy(new \yii\db\Expression('rand()'))
            ->all();
    }

}
