<?php

namespace backend\controllers;

use Yii;
use common\models\AdsProducts;
use backend\models\search\AdsProductsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AdsProductsController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }


    public function afterAction($action, $result)
    {
        if (!Yii::$app->view->params['admission']->promotional_products){
            throw new NotFoundHttpException();
        }else {
            return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
        }
    }

    public function actionIndex()
    {
        $searchModel = new AdsProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new AdsProducts();

        if ($model->load(Yii::$app->request->post())) {

            if(isset($_POST['products'])){
                $products  = $_POST['products'];
                if ($products[0] != "") {
                    AdsProducts::deleteAll(['category_id' => $model->category_id]);
                    foreach ($products as $v) {
                        $product = new AdsProducts();
                        $product->create($model->category_id, $v);
                    }
                }
            }

            return $this->redirect(['index']);

        }

        return $this->render('create', compact('model'));
    }



    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            AdsProducts::deleteAll(['category_id' => $model->category_id]);
            if(isset($_POST['products'])){
                $products  = $_POST['products'];
                if ($products[0] != "") {
                    foreach ($products as $v) {
                        $product = new AdsProducts();
                        $product->create($model->category_id, $v);
                    }
                }
            }

            return $this->redirect(['index']);

        }

        $products = AdsProducts::findAll(['category_id' => $model->category_id]);

        return $this->render('update', [
            'model'     => $model,
            'products'  => $products,
        ]);
    }

    public function actionDelete($id)
    {

        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }



    protected function findModel($id)
    {
        if (($model = AdsProducts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
