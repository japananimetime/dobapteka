<?php

namespace backend\controllers;

use common\models\Filial;
use common\models\FilialProduct;
use common\models\Geocoords;
use common\models\Orderedproduct;
use common\models\Products;
use Faker\Provider\Address;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_MemoryDrawing;
use PHPExcel_Writer_Excel2007;
use PhpOffice\PhpSpreadsheet\Shared\Drawing;
use Yii;
use common\models\Orders;
use backend\models\search\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class OrdersController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function afterAction($action, $result)
    {
        if (!Yii::$app->view->params['admission']->orders) {
            throw new NotFoundHttpException();
        } else {
            return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
        }
    }

    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $products = $model->products;
        $gift = $model->gift;
        $products = $this->addCount($products, $model);

        $addresses = Filial::findAll(['city_id' => 1]);
        $remainder = FilialProduct::getSomeByOrder($products);

        return $this->render('view', [
            'model' => $model,
            'products' => $products,
            'gift' => $gift,
            'addresses' => $addresses,
            'remainder' => $remainder,
        ]);
    }

    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $products = $this->getProducts($model->products);
        $gift = $this->getProducts($model->gift);
        $products = $this->addCount($products, $model);

        if ($model->load(Yii::$app->request->post())) {

            if (isset($_SESSION['products'])) {
                Orderedproduct::deleteAll('order_id=' . $id);
                $price = 0;
                foreach ($_SESSION['products'] as $v) {
                    if ($v->price == 0) $isGift = 1;
                    else {
                        $isGift = 0;
                        $price += $v->count * $v->getCalculatePrice();
                    }

                    Orderedproduct::createNew($id, $v->id, $v->count, $isGift);
                }

                $price = (int) ($price  * ((100 - $model->used_promo) / 100) * ((100 - $model->used_elderly_discount) / 100));
                if($model->typeDelivery == 1) $price += $model->deliverySum;
                $model->sum = $price - $model->used_bonus;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        unset($_SESSION['products']);

        return $this->render('update', compact('model', 'products', 'gift'));
    }



    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }



    protected function findModel($id)
    {
        $model = Orders::find()->where(['id' => $id, 'is_active' => 1])->one();
        if ($model) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    private function addCount($products, $model)
    {

        if ($products != null) {
            foreach ($products as $v) {
                foreach ($model->products as $pr) {
                    if ($v->id == $pr->product_id) {
                        $v->count = $pr->count;
                    }
                }
            }
        }

        return $products;
    }

    public function actionAddProduct()
    {


        if (Yii::$app->request->isAjax) {
            $ids = $_GET['products'];
            $order_id = $_GET['products_id'];
            $this->addProductToSession($order_id);

            // add new
            $products = Products::find()->where('id in ' . $ids)->all();
            foreach ($products as $v) {
                $check = true;
                foreach ($_SESSION['products'] as $key => $val) {
                    if ($v->id == $val->id) {
                        $check = false;
                        break;
                    }
                }

                if ($check) {
                    $v->count = 1;
                    array_push($_SESSION['products'], $v);
                }
            }
            // end add new;

            return $this->renderAjax('added-products', compact('order_id'));
        } else {
            throw new NotFoundHttpException();
        }
    }


    public function actionDeleteProduct()
    {

        if (Yii::$app->request->isAjax) {

            $id = $_GET['product_id'];
            $order_id = $_GET['order_id'];
            $this->addProductToSession($order_id);

            foreach ($_SESSION['products'] as $key => $val) {
                if ($val->id == $id) {
                    unset($_SESSION['products'][$key]);
                }
            }

            return $this->renderAjax('added-products', compact('order_id'));
        } else {
            throw new NotFoundHttpException();
        }
    }


    public function actionAppendProduct()
    {

        if (Yii::$app->request->isAjax) {

            $id = $_GET['product_id'];
            $order_id = $_GET['order_id'];
            $this->addProductToSession($order_id);

            $status = 0;
            $count = 1;
            $price = 0;

            foreach ($_SESSION['products'] as $k => $v) {
                if ($v->id == $id) {
                    $sum = FilialProduct::find()
                        ->distinct()
                        ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                        ->where('filial.status = 2')
                        ->andWhere(['filial_product.product_id' => $id])
                        ->sum('amount');
                    if ($sum >= $_SESSION['products'][$k]['count'] + 1) {
                        $_SESSION['products'][$k]['count'] += 1;
                        $status = 1;
                    }

                    $count = $_SESSION['products'][$k]['count'];
                    $price = $_SESSION['products'][$k]->calculatePrice * $count;
                    break;
                }
            }

            $array = [
                'status' => $status,
                'count' => $count,
                'price' => $price,
            ];

            return json_encode($array);
        } else {
            throw new NotFoundHttpException();
        }
    }





    public function actionPrependProduct()
    {

        if (Yii::$app->request->isAjax) {

            $id = $_GET['product_id'];
            $order_id = $_GET['order_id'];
            $this->addProductToSession($order_id);

            $count = 1;
            $price = 0;

            foreach ($_SESSION['products'] as $k => $v) {
                if ($v->id == $id) {
                    if ($_SESSION['products'][$k]->count > 1) {
                        $_SESSION['products'][$k]['count'] -= 1;
                        $count = $_SESSION['products'][$k]['count'];
                    }

                    $price = $_SESSION['products'][$k]->calculatePrice * $count;
                    break;
                }
            }

            $array = [
                'count' => $count,
                'price' => $price,
            ];

            return json_encode($array);
        } else {
            throw new NotFoundHttpException();
        }
    }




    private function addProductToSession($order_id)
    {

        if (!isset($_SESSION['products'])) {

            $_SESSION['products'] = array();
            //add old
            $model = Orderedproduct::findAll(['order_id' => $order_id, 'isGift' => 0]);

            if ($model != null) {
                $arr = '(';
                foreach ($model as $v) {
                    $arr .= $v->product_id . ',';
                }
                $arr = substr($arr, 0, strlen($arr) - 1);
                $arr .= ')';

                $products = Products::find()->where('id in ' . $arr)->all();

                foreach ($products as $v) {
                    foreach ($model as $pr) {
                        if ($v->id == $pr->product_id) {
                            if ($pr->isGift == 1) {
                                $v->price = 0;
                            }
                            $v->count = $pr->count;
                        }
                    }
                }

                foreach ($products as $v) {
                    array_push($_SESSION['products'], $v);
                }
            }
        }
    }





    public function actionGetNearlyProducts()
    {
        $response = array();
        if (Yii::$app->request->isAjax) {
            $data =  Yii::$app->request->post()['data'];
            $view = Yii::$app->request->post()['view'];
            for ($i = 0; $i < count($data); $i++) {

                $product_id = $data[$i][0];
                $model = Products::findOne($product_id);

                $count =  $data[$i][1];
                $pharmacy_number = $data[$i][3];
                $pharmacy = $pharmacy_number.', '.$data[$i][2];
                $name = $model->name;
                $image = $model->getImage();

                $response[$pharmacy][] = [
                    "id" => $product_id, "product" => $name,
                    "image" => $image, "count" => $count, 'path' => $model->path
                ];
            }

            return $this->renderAjax($view, compact('response'));
        } else {
            throw new NotFoundHttpException();
        }
    }




    public function actionExportFile()
    {

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(40);

        $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('J')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('K')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('L')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('M')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('N')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('O')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('P')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('Q')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q1:R1');
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', '№')
            ->setCellValue('B1', 'Номер заказа')
            ->setCellValue('C1', 'ФИО клиента ')
            ->setCellValue('D1', 'Тип заказа ')
            ->setCellValue('E1', 'Тип доставки ')
            ->setCellValue('F1', 'Статус оплаты ')
            ->setCellValue('G1', 'Статус доставки ')
            ->setCellValue('H1', 'Цена заказа')
            ->setCellValue('I1', 'Подготовить сдачу с суммы')
            ->setCellValue('J1', 'Использованная скидка по промокоду')
            ->setCellValue('K1', 'Использованная скидка для пенсионеров')
            ->setCellValue('L1', 'Использованный бонус')
            ->setCellValue('M1', 'Цена доставки')
            ->setCellValue('N1', 'Адрес доставки')
            ->setCellValue('O1', 'Время доставки')
            ->setCellValue('P1', 'Дата заказа')
            ->setCellValue('Q1', 'Продукты заказа');


        $model = Orders::find()->where(['is_active' => 1])->orderBy('id desc')->all();
        $m = 1;
        $sum = 0;
        foreach ($model as $v) {
            $m++;
            $sum++;
            $products = $this->getProducts($v->products);
            $products = $this->addCount($products, $v);

            if ($v->user_id) $fio = $v->userProfile->fio;
            else $fio = $v->fio;

            $objPHPExcel->getActiveSheet()
                ->setCellValue('A' . $m, $sum)
                ->setCellValue('B' . $m, $v->id)
                ->setCellValue('C' . $m, $fio)
                ->setCellValue('D' . $m, LabelType::statusLabelValue($v->typePay))
                ->setCellValue('E' . $m, LabelDeliveryMethod::statusLabelValue($v->typeDelivery))
                ->setCellValue('F' . $m, LabelPay::statusLabelValue($v->statusPay))
                ->setCellValue('G' . $m, LabelProgress::statusLabelValue($v->statusProgress))
                ->setCellValue('H' . $m, $v->sum . ' тг')
                ->setCellValue('I' . $m, $v->change . ' тг')
                ->setCellValue('J' . $m, $v->used_promo . ' %')
                ->setCellValue('K' . $m, $v->used_elderly_discount    . ' %')
                ->setCellValue('L' . $m, $v->used_bonus . ' тг')
                ->setCellValue('P' . $m, $v->created);


            if ($v->deliverySum) {
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('M' . $m, $v->deliverySum . ' тг');
            } else {
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('M' . $m, "(не задано)");
            }

            if ($v->address) {
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('N' . $m, $v->address);
            } else {
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('N' . $m, "(не задано)");
            }


            if ($v->time) {
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('O' . $m, $v->time . ' минут');
            } else {
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('O' . $m, "(не задано)");
            }


            // ORDERED PRODUCTS

            if ($products != null) {
                $n = $m - 1;
                foreach ($products as $v) {
                    $n++;
                    $m = $n;
                    $objPHPExcel->getActiveSheet()
                        ->setCellValue('Q' . $n, $v->name);
                    $objPHPExcel->getActiveSheet()
                        ->setCellValue('R' . $n, $v->count);
                    $img = $v->getImage();
                }
            }
        }


        $filename = "Orders_" . date('d-m-Y h:i', time()) . ".xlsx";


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . basename($filename));
        header('Cache-Control: must-revalidate');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $filePath = __DIR__ . "/" . rand(0, getrandmax()) . rand(0, getrandmax()) . ".xlsx";
        $objWriter->save($filePath);
        readfile($filePath);
    }

    public function getProducts($model)
    {
        if ($model != null) {
            $arr = '(';
            foreach ($model as $v) {
                $arr .= $v->product_id . ',';
            }
            $arr = substr($arr, 0, strlen($arr) - 1);
            $arr .= ')';
            $products = Products::find()->where('id in ' . $arr)->all();
        } else {
            $products = null;
        }
        return $products;
    }
}
