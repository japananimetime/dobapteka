<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Список рассылки СМС-уведомлений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-notification-lists-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'phone',
                'value' => function($data){
                    return $data->phone;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' =>  \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'created_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
