<?php

use yii\helpers\Html;

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Список рассылки СМС-уведомлений', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-notification-lists-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
