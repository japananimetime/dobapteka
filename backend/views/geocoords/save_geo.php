<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="//yastatic.net/jquery/1.8.3/jquery.min.js"></script>
<div id="map" style="width: 80%; height: 600px;"></div>
<div id="viewContainer"></div>
<script>
    ymaps.ready(init);

    var coords = [];
    var id_poligon = 0;

    function init() {
        var myMap = new ymaps.Map("map", {
            center: [43.238293, 76.945465],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });

        <?foreach($model as $v){?>
        <?$array = unserialize($v->coords);?>
        var coords = [];
        <?foreach($array[0] as $arr){?>
        coords.push([<?=$arr[0]?>, <?=$arr[1]?>]);
        <?}?>
        var myPolygon<?=$v->id?> = new ymaps.Polygon([coords], {}, {
            editorDrawingCursor: "crosshair",
            fillColor: '#00FF00',
            strokeColor: '#0000FF',
            strokeWidth: 5
        });
        myMap.geoObjects.add(myPolygon<?=$v->id?>);


        <?}?>

        var myPolygon = new ymaps.Polygon([], {}, {
            // Курсор в режиме добавления новых вершин.
            editorDrawingCursor: "crosshair",
            // Максимально допустимое количество вершин.
            //editorMaxPoints: 5,
            // Цвет заливки.
            fillColor: '#00FF00',
            // Цвет обводки.
            strokeColor: '#0000FF',
            // Ширина обводки.
            strokeWidth: 5
        });

        myMap.geoObjects.add(myPolygon);

        //Создание
        var stateMonitor = new ymaps.Monitor(myPolygon.editor.state);
        stateMonitor.add("drawing", function (newValue) {
            myPolygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
        });

        myPolygon.editor.startDrawing();

        myMap.geoObjects.add(myPolygon);
        myPolygon.editor.startDrawing();
        myPolygon.editor.startEditing();

        myPolygon.editor.events.add(['vertexadd'], function(e){
            coords = myPolygon.geometry.getCoordinates();
        });

        $('body').on('click', '.save_geo', function () {
            var sum = $('.summ').val();
            var delivery_time =  $('.name').val();
            if(sum == "") sweetAlert("Ошибка!", "Необходимо заполнить «Цена доставки»", "error");
            else if(delivery_time == "") sweetAlert("Ошибка!", "Необходимо заполнить «Время доставки»", "error");
            else {
                $.ajax({
                    url: '/admin/geocoords/save_geo',
                    method: 'GET',
                    data: {coords: coords, summ: sum, name: delivery_time},
                    success: function (response) {
                        $('.create_geoobject').html(response);
                        $('.summ').val('');
                        $('.name').val('');
                    },
                    error: function () {

                    }
                });
            }
        });
    }
</script>
