<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Подарки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return $model->typeName;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return $model->productName;
                },
                'format' => 'raw',
            ],
            'from_price',
            'to_price',
            [
                'attribute' => 'products',
                'value' => function ($model) {
                    return $model->availableProducts;
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
