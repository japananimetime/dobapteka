<?php

use yii\helpers\Html;

$this->title = 'Редактирование : ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Администратор', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'admission' => $admission,
        'from' => "update"
    ]) ?>
</div>
