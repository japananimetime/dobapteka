<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = ' При обновлении Товар Месяца';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="trigger-tovar-month-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
