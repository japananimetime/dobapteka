<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="contact-form">

    <div class="contact-form" >

        <?php $form = ActiveForm::begin(); ?>


        <div class="col-md-12 pl-0 pr-0">
            <div class="form-group" style="float: right;margin-top:7px;">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
            <ul id="myTab" role="tablist" class="nav nav-tabs">
                <li class="nav-item active">
                    <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
                </li>
                <li class="nav-item">
                    <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Cоциальные сети</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
                <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">

                    <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

                </div>
                <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'address')->textarea() ?>

                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
