<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'surname',
                'value' => $model->surname,
                'format' => 'raw',
            ],

            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'telephone',
                'value' => $model->telephone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'city',
                'value' => $model->city,
                'format' => 'raw',
            ],
            [
                'attribute' => 'comment',
                'value' => $model->comment,
                'format' => 'raw',
            ],


            'created_at',
        ],
    ]) ?>

</div>
