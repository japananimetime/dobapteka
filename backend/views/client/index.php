<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Экспорт в Excel', ['export-file'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'username',
                'value' => function ($data) {
                    return $data->username;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'email',
                'value' => function($model){
                    return $model->email;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return DateTime::createFromFormat('U', $model->created_at)->format('Y-m-d H:i:s');
                },
                'format' => 'raw',
            ],


            ['class' => 'yii\grid\ActionColumn'],

            ],
    ]); ?>
</div>
