<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',

            [
                'attribute' => 'ФИО',
                'value' => function($model){
                    return $model->fio;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'username',
                'value' => function($model){
                    return $model->username;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => function($model){
                    return $model->email;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'Адрес',
                'value' => function($model){
                    return $model->addressName;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'Дата рождения',
                'value' => function($model){
                    return $model->dateOfBirth;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return DateTime::createFromFormat('U', $model->created_at)->format('Y-m-d H:i:s');
                },
                'format' => 'raw',
            ],

        ]
    ]) ?>







</div>
