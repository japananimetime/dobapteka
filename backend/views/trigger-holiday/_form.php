<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="trigger-holiday-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo DatePicker::widget([
        'name'=>'TriggerHoliday[date]',
        'value' => $model->date,
        'options' => ['placeholder' => 'Выберите дату...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'startView'=>'months',
            'minViewMode'=>'days',
            'todayHighlight' => true
        ],

    ]);
    ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => [
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
