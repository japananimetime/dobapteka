<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'В празднике';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trigger-holiday-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'date',
                'value' => function($data){
                    return $data->getDate();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'content',
                'value' => function ($data) {
                    return $data->content;
                },
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
