<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Юридическая информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="legal-information-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',

            [
                'attribute' => 'name',
                'value' => function($model){
                    return $model->name;
                },
                'format' => 'raw',
            ],
            [
                'format' => 'raw',
                'attribute' => 'file',
                'value' => function($model){
                    if($model->file != null){
                        return Html::a('Посмотреть файл', $model->getFile(), ['target' => '_blank']);
                    }else{
                        return '';
                    }
                },
            ],

            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
