<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>


    <? if($model->typeDelivery == 1):?>
        <?= $form->field($model, 'statusProgress')->dropDownList(\backend\controllers\LabelProgress::statusList()) ?>
    <? else:?>
        <?= $form->field($model, 'statusProgress')->dropDownList(\backend\controllers\LabelDelivery::statusList()) ?>
    <? endif;?>

    <?= $form->field($model, 'statusPay')->dropDownList([0 => 'Не оплачено', 1 => 'Оплачено']) ?>

    <?= $form->field($model, 'typeDelivery')->dropDownList(\backend\controllers\LabelDeliveryMethod::statusList()) ?>

    <?= $form->field($model, 'sum')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_bonus')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_promo')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_elderly_discount')->textInput(['disabled'=>'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<br><br>
<div>
    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Продукты заказа:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="orderedProducts" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Количества</th>
                    <th>Цена</th>
                    <? if($model->typeDelivery == 1):?>
                    <th>Доступные действия</th>
                    <? endif;?>
                </tr>
                </thead>
                <tbody>
                <? if($products != null):?>
                    <? foreach ($products as $v):?>

                        <tr>
                            <td><a href="/admin/products/view?id=<?=$v->id;?>">
                                    <img src="<?=$v->getImage();?>" width="50"> <?=$v->name;?></a></td>
                            <td id = "count_<?=$v->id;?>"><?=$v->count;?></td>
                            <td id = "price_<?=$v->id;?>"><?=$v->calculatePrice*$v->count;?></td>
                            <? if($model->typeDelivery == 1):?>
                            <td>
                                <button class="btn btn-success" onclick="appendOrderedProduct(<?=$v->id;?>, <?=$model->id;?>)">+</button>
                                <button class="btn btn-default" onclick="prependOrderedProduct(<?=$v->id;?>, <?=$model->id;?>)">-</button>
                                <button class="btn btn-danger" onclick="deleteOrderedProduct(<?=$v->id;?>, <?=$model->id;?>)">x</button>
                            </td>
                            <? endif;?>
                        </tr>
                    <? endforeach;?>
                <? endif;?>

                </tbody>
            </table>
        </div>
    </div>


    <? if($model->typeDelivery == 1):?>

    <div class="form-group">

        <h2>Добавить продукты</h2>
        <select class="form-control" multiple="multiple" data-placeholder="Выберите продукта"  id="orders"
               name="OrderedProduct[product][]">
        </select>


    </div>

    <button type="button" class="btn btn-success" id="addProduct">Добавить продукты</button>
    <? endif;?>
</div>



<script>

    $('body').on('click', '#addProduct', function () {

        var products = $('select[name="OrderedProduct[product][]"]').val();
        if(products == ''){
            alert('Выберите продукта.');
        }else {

            var res = '(';
            for(var i=0;i<products.length;i++){
                res += products[i]+',';
            }
            res = res.substring(0, res.length - 1);
            res += ')';
            products = res;

            $.ajax({
                url: "/admin/orders/add-product",
                type: "GET",
                data: {products:products,products_id:<?=$model->id;?>},
                success: function(data){
                    $('#orderedProducts').html(data);
                },
                error: function () {
                    alert('Что-то пошло не так.');
                }
            });
        }
    });


    function deleteOrderedProduct(product_id, order_id){

        $.ajax({
            url: "/admin/orders/delete-product",
            type: "GET",
            data: {product_id:product_id, order_id:order_id},
            success: function(data){
                $('#orderedProducts').html(data);
            },
            error: function () {
                alert('Что-то пошло не так.');
            }
        });
    }



    function appendOrderedProduct(product_id, order_id){

        $.ajax({
            url: "/admin/orders/append-product",
            type: "GET",
            dataType: "json",
            data: {product_id:product_id, order_id:order_id},
            success: function(data){
                if(data.status == 1){
                    $('#count_'+product_id).html(data.count);
                    $('#price_'+product_id).html(data.price);
                }else{
                    alert('Закончился')
                }
            },
            error: function () {
                alert('Что-то пошло не так.');
            }
        });
    }




    function prependOrderedProduct(product_id, order_id){

        $.ajax({
            url: "/admin/orders/prepend-product",
            type: "GET",
            dataType: "json",
            data: {product_id:product_id, order_id:order_id},
            success: function(data){
                $('#count_'+product_id).html(data.count);
                $('#price_'+product_id).html(data.price);
            },
            error: function () {
                alert('Что-то пошло не так.');
            }
        });
    }



    //function mockData() {
    //
    //    return [
    //        <?// foreach ($all_products as $k => $v):?>
    //        {
    //            "id": <?//=$v->id;?>//,
    //            "text": "<?//=str_replace('"','',$v->name);?>//"
    //        },
    //        <?// endforeach;?>
    //
    //    ];
    //}



</script>
