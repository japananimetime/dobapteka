
<thead>
<tr>
    <th>Товар</th>
    <th>Количества</th>
    <th>Цена</th>
    <th>Доступные действия</th>
</tr>
</thead>
<tbody>
<? if($_SESSION['products'] != null):?>
    <? foreach ($_SESSION['products'] as $v):?>
        <tr>
            <td><a href="/admin/products/view?id=<?=$v->id;?>">
                    <img src="<?=$v->getImage();?>" width="50">  <?=$v->name;?></a></td>
            <td id = "count_<?=$v->id;?>"><?=$v->count;?></td>
            <td id = "price_<?=$v->id;?>"><?=$v->calculatePrice*$v->count;?></td>
            <td>
                <button class="btn btn-success" onclick="appendOrderedProduct(<?=$v->id;?>, <?=$order_id;?>)">+</button>
                <button class="btn btn-default" onclick="prependOrderedProduct(<?=$v->id;?>, <?=$order_id;?>)">-</button>
                <button class="btn btn-danger" onclick="deleteOrderedProduct(<?=$v->id;?>, <?=$order_id;?>)">x</button>
            </td>
        </tr>
    <? endforeach;?>
<? endif;?>
</tbody>
