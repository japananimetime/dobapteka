<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Текст по сайту', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'text',
                'value' => $model->text,
                'format' => 'raw',
            ],
            [
                'attribute'=>'type_id',
                'filter'=>\common\models\TextType::getList(),
                'value'=>function($model){
                    return $model->type->name;
                },
            ],
        ],
    ]) ?>

</div>
