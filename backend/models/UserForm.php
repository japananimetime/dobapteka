<?php

namespace backend\models;

use common\models\User;
use yii\base\Model;

class UserForm extends Model
{


    public $password;
    public $username;

    public function rules()
    {
        return [

            [['username'], 'required'],
            [['username'], 'unique', 'targetClass'=>'common\models\User'],

            [['password'], 'required'],
            [['password'], 'string', 'min' => 6],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Пароль',
            'username' => 'Логин',
            'created_at' => 'Дата создания',
        ];
    }


    public function signUp()
    {
        if (!$this->validate()) {
            return null;
        }


        $user = new User();
        $user->username = $this->username;
        $user->role = 2;
        $user->generateAuthKey();
        $user->setPassword($this->password);
        return $user->save(false) ? $user : null;
    }
}
