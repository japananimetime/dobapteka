<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DiscountEarliestPersons;

class DiscountEarliestPersonsSearch extends DiscountEarliestPersons
{
    public function rules()
    {
        return [
            [['id', 'age', 'discount'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = DiscountEarliestPersons::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'age' => $this->age,
            'discount' => $this->discount,
        ]);

        return $dataProvider;
    }
}
