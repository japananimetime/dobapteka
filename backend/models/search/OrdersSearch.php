<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

class OrdersSearch extends Orders
{
    public function rules()
    {
        return [
            [['id', 'statusPay','statusProgress', 'sum', 'typePay', 'typeDelivery', 'is_active'], 'integer'],
            [['created', 'user_id', 'fio'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Orders::find()
            ->leftJoin('user_profile', '`user_profile`.`user_id` = `orders`.`user_id`');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'orders.id' => $this->id,
            'orders.is_active' => $this->is_active,
            'orders.statusPay' => $this->statusPay,
            'orders.statusProgress' => $this->statusProgress,
            'orders.sum' => $this->sum,
            'orders.typeDelivery' => $this->typeDelivery,
            'orders.typePay' => $this->typePay,
            'orders.created' => $this->created,
        ]);

        if($this->user_id != null){
            $query->andWhere("orders.fio LIKE '%$this->user_id%' OR user_profile.fio LIKE '%$this->user_id%'");
        }

        $query->andWhere(['orders.is_active' => 1]);
        $query->orderBy('orders.id DESC');

        return $dataProvider;
    }
}
